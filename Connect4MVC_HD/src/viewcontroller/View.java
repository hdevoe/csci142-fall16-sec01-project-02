package viewcontroller;


import java.awt.Color;
import java.awt.Font;
import java.awt.Frame;
import java.awt.GridLayout;
import java.awt.Image;
import java.awt.Panel;
import java.awt.Toolkit;
import java.lang.reflect.Method;

import javax.swing.JScrollPane;
import javax.swing.JTextArea;
import javax.swing.JTextField;
import javax.swing.text.BadLocationException;
import javax.swing.text.DefaultCaret;

import connectmodel.PieceType;

/**
 * The View class gives the player a GUI in order to interact with the game.
 * @author hudson
 *
 */
@SuppressWarnings("serial")
public class View extends Frame
{
    //Properties
	
	//GameBoard components
    private int myNumColumns;
    private int myNumRows;
    private Can[][] myPiece;
    private Panel myPiecesPanel;
    private ButtonListener[][] myPieceListener;
    
    //TextFields for player names
    private JTextField myPlayer1Field;
    private JTextField myComPlayerField;
    
    //TextFields for player wins
    private JTextField myPlayer1WinsField;
    private JTextField myComPlayerWinsField;
    
    //TextField for number of ties
    private JTextField myNumTiesField;
    
    //TextArea for myGameLog
    private JTextArea myGameLog;
    private JScrollPane myGameLogPane;
    
    //Controller object
    private Controller myController;
    
    //Images for pieces
    private Image myEmptyPiece;
    private Image myBlackPiece;
    private Image myGreenPiece;
    private Image myRedPiece;
    private Image myYellowPiece;
    
    //color variables
    private Color myFontColor = Color.white;
    private Color myBackgroundColor = Color.cyan;

    /**
     * Constructs view object with a given controller
     * @param controller
     */
    public View(Controller controller)
    {
        String value;
        this.setSize(1280,720);
        this.setLayout(null);
        this.setBackground(myBackgroundColor);
        
        myController = controller;
       
        //gets the number of columns and rows from gameboard class
        myNumColumns = myController.getMyGameEngine().getGameBoard().getNumColumns();
        myNumRows = myController.getMyGameEngine().getGameBoard().getNumRows();
        

        //Import images from images folder
        myEmptyPiece = Toolkit.getDefaultToolkit().getImage("images/EmptyPiece.png");
        myBlackPiece = Toolkit.getDefaultToolkit().getImage("images/BlackPiece.png");
        myGreenPiece = Toolkit.getDefaultToolkit().getImage("images/GreenPiece.png");
        myRedPiece = Toolkit.getDefaultToolkit().getImage("images/RedPiece.png");
        myYellowPiece = Toolkit.getDefaultToolkit().getImage("images/YellowPiece.png");
        
        //Intialize my pieces
        myPiece = new Can[myNumColumns][myNumRows];
        myPieceListener = new ButtonListener[myNumColumns][myNumRows];
        myPiecesPanel = new Panel(new GridLayout(myNumRows,myNumColumns));
        myPiecesPanel.setSize(myNumColumns*64, myNumRows*64);
        myPiecesPanel.setLocation((this.getWidth()/2) - (myPiecesPanel.getWidth()/2),(this.getHeight()* 2/5) -( myPiecesPanel.getHeight()/2));
       
        for (int y = 0; y < myNumRows; y++)
        {
        	for(int x=0; x < myNumColumns; x++)
        	{
        	
        		myPiece[x][y] = new Can(myEmptyPiece);
        		myPiecesPanel.add(myPiece[x][y]);
        	}
        } 
             
        //Create font
        Font font1 = new Font("SansSerif", Font.BOLD, 20);
        
        //Add Player1 Text Field
        value = myController.getMyGameEngine().getMyPlayer1().getName();
        myPlayer1Field = new JTextField(value);
        myPlayer1Field.setSize(200,80);
        myPlayer1Field.setLocation((this.getWidth()/10), this.getHeight()/10);
        myPlayer1Field.setBorder(javax.swing.BorderFactory.createEmptyBorder());
        myPlayer1Field.setFont(font1);
        myPlayer1Field.setForeground(myFontColor);
        myPlayer1Field.setEditable(false);
        myPlayer1Field.setBackground(myBackgroundColor);
        myPlayer1Field.setHorizontalAlignment(JTextField.CENTER);
        
        
        //Add Comp Player Text Field
        value = myController.getMyGameEngine().getComPlayer().getName();
        myComPlayerField = new JTextField(value);
        myComPlayerField.setSize(200, 80);
        myComPlayerField.setLocation((this.getWidth()* 8/10) - (myComPlayerField.getWidth()/2), this.getHeight()/10);
        myComPlayerField.setBorder(javax.swing.BorderFactory.createEmptyBorder());
        myComPlayerField.setFont(font1);
        myComPlayerField.setForeground(myFontColor);
        myComPlayerField.setEditable(false);
        myComPlayerField.setBackground(myBackgroundColor);
        myComPlayerField.setHorizontalAlignment(JTextField.CENTER);

        //Add Player1Wins Text Field
        value = "Wins: " + Integer.toString(myController.getMyGameEngine().getMyPlayer1().getNumWins());
        myPlayer1WinsField = new JTextField(value);
        myPlayer1WinsField.setSize(200, 80);
        myPlayer1WinsField.setLocation(this.getWidth()/10, this.getHeight() * 2/10);
        myPlayer1WinsField.setBorder(javax.swing.BorderFactory.createEmptyBorder());
        myPlayer1WinsField.setFont(font1);
        myPlayer1WinsField.setForeground(myFontColor);
        myPlayer1WinsField.setEditable(false);
        myPlayer1WinsField.setBackground(myBackgroundColor);
        myPlayer1WinsField.setHorizontalAlignment(JTextField.CENTER);
        
        //Add ComPlayerWins Text Field
        value = "Wins: " + Integer.toString(myController.getMyGameEngine().getComPlayer().getNumWins());
        myComPlayerWinsField = new JTextField(value);
        myComPlayerWinsField.setSize(200, 80);
        myComPlayerWinsField.setLocation((this.getWidth() * 8/10) - (myComPlayerWinsField.getWidth()/2), this.getHeight() * 2/10);
        myComPlayerWinsField.setBorder(javax.swing.BorderFactory.createEmptyBorder());
        myComPlayerWinsField.setFont(font1);
        myComPlayerWinsField.setForeground(myFontColor);
        myComPlayerWinsField.setEditable(false);
        myComPlayerWinsField.setBackground(myBackgroundColor);
        myComPlayerWinsField.setHorizontalAlignment(JTextField.CENTER);
        
        //Add myTies Text Field
        value = "Ties: " + Integer.toString(myController.getMyGameEngine().getGameBoard().getNumTies());
        myNumTiesField = new JTextField(value);
        myNumTiesField.setSize(200, 80);
        myNumTiesField.setLocation((this.getWidth() /2) - (myNumTiesField.getWidth()/2), this.getHeight() /20);
        myNumTiesField.setBorder(javax.swing.BorderFactory.createEmptyBorder());
        myNumTiesField.setFont(font1);
        myNumTiesField.setForeground(myFontColor);
        myNumTiesField.setEditable(false);
        myNumTiesField.setBackground(myBackgroundColor);
        myNumTiesField.setHorizontalAlignment(JTextField.CENTER);
        
        //Add myGameLog Text Area
        myGameLog = new JTextArea();
        myGameLog.setSize(600,100);
        myGameLog.setFont(font1);
        myGameLog.setBackground(myBackgroundColor);
        myGameLog.setForeground(myFontColor);
        myGameLog.setEditable(false);
        myGameLog.setBorder(javax.swing.BorderFactory.createEmptyBorder());
        
        //Add myGameLog to myGameLogScrollPane
        myGameLogPane = new JScrollPane(myGameLog);
        myGameLogPane.setSize(600,200);
        myGameLogPane.setBorder(javax.swing.BorderFactory.createEmptyBorder());
        myGameLogPane.setLocation((this.getWidth() /2) - (myGameLogPane.getWidth()/2), this.getHeight() * 7/10);
        
        this.add(myPiecesPanel);
        this.add(myPlayer1Field);
        this.add(myComPlayerField);
        this.add(myPlayer1WinsField);
        this.add(myComPlayerWinsField);
        this.add(myNumTiesField);
        this.add(myGameLogPane);
        
        this.setVisible(true);
        this.addWindowListener(new AWindowListener());
        this.associateListeners(myController);
    }
    
    /**
     * Associates each component's listener with the controller
     * and the correct method to invoke when triggered.
     *
     * <pre>
     * pre:  the controller class has be instantiated
     * post: all listeners have been associated to the controller
     *       and the method it must invoke
     * </pre>
     */
    public void associateListeners(Controller controller)
    {
        Class<? extends Controller> controllerClass;
        Method placePiece;
        Class<?>[] classArgs;

        controllerClass = controller.getClass();
        
        placePiece = null;

        classArgs = new Class[1];
        
        try
        {
           classArgs[0] = Class.forName("java.lang.Integer");
        }
        catch(ClassNotFoundException e)
        {
           String error;
           error = e.toString();
           System.out.println(error);
           
        }
        
        
        try
        {
           placePiece = controllerClass.getMethod("placePiece",classArgs);      
        }
        catch(NoSuchMethodException exception)
        {
           String error;

           error = exception.toString();
           System.out.println(error);
           
        }
        catch(SecurityException exception)
        {
           String error;

           error = exception.toString();
           System.out.println(error);
           
        }
        
        Integer[] args;

        
        for (int y = 0; y < myNumRows; y++)
        {
        	for (int x=0; x < myNumColumns; x++)
            {
        		args = new Integer[1];
        		args[0] = new Integer(x);
        		myPieceListener[x][y] = new ButtonListener(myController, placePiece, args);
        		myPiece[x][y].addMouseListener(myPieceListener[x][y]);
        	}

        }
    }
    
    /**
     * Changes Image from empty to filled with given piece type
     * @author hudson
     * @param column
     * @param row
     * @param piecetype
     */
    public void changeImage(int column, int row, PieceType piecetype)
    {
    	if (column < myNumColumns && column >= 0 && row >= 0 && row <= myNumRows)
    	{
    		if(piecetype == PieceType.GREEN) myPiece[column][row].setImage(myGreenPiece);
    		if(piecetype == PieceType.RED) myPiece[column][row].setImage(myRedPiece);
    		if(piecetype == PieceType.YELLOW) myPiece[column][row].setImage(myYellowPiece);
    		if(piecetype == PieceType.BLACK) myPiece[column][row].setImage(myBlackPiece);
    	}
    }
    
    /**
     * Changes all images back to empty piece image
     * @author hudson
     */
    public void wipeBoard()
    {
    	for (int y = 0; y < myNumRows; y++)
        {
        	for(int x=0; x < myNumColumns; x++)
            {
        		myPiece[x][y].setImage(myEmptyPiece);
        	}
        } 
    }

    /**
     * Updates Player1TextField with the String text.
     *
     * @param text the text string to use in updating the text field
     * @author hudson
     */
    public void setPlayer1TextField(String text)
    {
        myPlayer1Field.setText(text);
    }
    
    /**
     * Updates Player1Wins TextField with the int wins.
     *
     * @param text the text string to use in updating the text field
     * @author hudson
     */
    public void setPlayer1WinsTextField(int wins)
    {
        myPlayer1WinsField.setText("Wins: " + wins);
    }
    
    /**
     * Updates Com Player Wins TextField with the int wins.
     *
     * @param text the text string to use in updating the text field
     * @author hudson
     */
    public void setComPlayerWinsTextField(int wins)
    {
        myComPlayerWinsField.setText("Wins: " + wins);
    }
    
    /**
     * Updates Num Ties TextField with the int wins.
     *
     * @param text the text string to use in updating the text field
     * @author hudson
     */
    public void setNumTiesTextField(int ties)
    {
    	myNumTiesField.setText("Ties: " + ties);
    }
    
    /**
     * Appends String text to the myGameLog text area
     *
     *@author hudson
     * @param text the text string to use in updating the text area
     */
    public void appendLogTextArea(String string)
    {
    	myGameLog.append(string + "\n");
    	
    	DefaultCaret caret = (DefaultCaret)myGameLog.getCaret();
    	 caret.setUpdatePolicy(DefaultCaret.ALWAYS_UPDATE);
    }
    
    /**
     * Clears the log text area
     * @author hudson
     */
    public void clearLogTextArea()
    {
    	myGameLog.setText(null);
    }
}