package viewcontroller;

public class MainMVC {

    // Properties
    private Controller myController;
    
    // Methods
    public static void main(String[] args)
    {
        new MainMVC();
    }
    
    public MainMVC()
    {
        setController(new Controller());
    }

	public void setController(Controller controller) 
	{
		myController = controller;
	}

	public Controller getController() 
	{
		return myController;
	}
}
