package viewcontroller;

import javax.swing.JLabel;
import javax.swing.JList;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JTextField;
import javax.swing.ListSelectionModel;

import connectmodel.GameBoard;
import connectmodel.GameEngine;
import connectmodel.PieceType;
import connectmodel.Player;

/**
 * Controller class is responsible for the relationship between GameEngine model and the view
 */
public class Controller
{
	//Properties
    private View myView;
    private GameEngine myGameEngine;
    
    //Methods
    
    /**
     * The controller function makes a new controller with the name and type given in the popup
     */
    public Controller()
    {
    	
    	JTextField nameField = new JTextField(5);
        String name = "";
        String type = "";
        
        String[] types = new String[]{"Black", "Red", "Green", "Yellow"};
        JList list = new JList(types);
        list.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
        list.setSelectedIndex(0);
        
        JPanel myPanel = new JPanel();
        myPanel.add(new JLabel("Enter Name:"));
        myPanel.add(nameField);
        myPanel.add(new JLabel("Select PieceType:"));
        myPanel.add(list);
        
        int result = JOptionPane.showConfirmDialog(null, myPanel, 
                 "Start Up", JOptionPane.OK_CANCEL_OPTION);
        if (result == JOptionPane.OK_OPTION) {
           name = nameField.getText();
           type = (String)list.getSelectedValue();
        } else if (result == JOptionPane.CANCEL_OPTION)
        {
        	System.exit(0);
        }

        
    	PieceType[] myPieceTypes = new PieceType[2];
    	myPieceTypes[0] = PieceType.type(type);
    	
    	if (myPieceTypes[0] == PieceType.BLACK)
    	{
    		myPieceTypes[1] = PieceType.RED;
    	} else 
    		
    	if (myPieceTypes[0] == PieceType.RED)
    	{
    		myPieceTypes[1] = PieceType.BLACK;
    	} else 
    		
    	if (myPieceTypes[0] == PieceType.GREEN)
    	{
    		myPieceTypes[1] = PieceType.YELLOW;
    	} else 
    		
    	if (myPieceTypes[0] == PieceType.YELLOW)
    	{
    		myPieceTypes[1] = PieceType.GREEN;
    	}
    	
    	Player myPlayer1 = new Player(name, myPieceTypes[0]);
    	GameBoard myGameBoard = new GameBoard(6,7,4,myPieceTypes);
        myGameEngine = new GameEngine(myPlayer1, myGameBoard);
        myView = new View(this);
    }
    
    /**
     * 
     * The placePiece function takes in a column and places a piece at the correct spot for the given 
     * @author hudson
     * @param Integer iColumn
     */
    public void placePiece(Integer iColumn)
    {
    	if(!myGameEngine.getGameBoard().getIsAWin() && !myGameEngine.getGameBoard().getIsATie())
    	{
    		if (myGameEngine.getPlayerUp() == myGameEngine.getMyPlayer1())
    		{
    			int row, column;
    			column = (int)iColumn;
    			if (myGameEngine.placePiece(column)) 
    			{
    				row = (int)myGameEngine.getGameBoard().getLastPoint().getY() +1;
    				myView.changeImage(column, myGameEngine.getGameBoard().getNumRows()-row, myGameEngine.getPlayerUp().getPieceType());
    				myView.appendLogTextArea(myGameEngine.getPlayerUp().getName() + " places piece at " + (int)myGameEngine.getGameBoard().getLastPoint().getX() + ", " + (int)myGameEngine.getGameBoard().getLastPoint().getY());
    				if (myGameEngine.getGameBoard().checkIfWin()) 
    				{	
    					myGameEngine.getPlayerUp().incrementNumWins();
    					myView.setPlayer1WinsTextField(myGameEngine.getMyPlayer1().getNumWins());
    					myView.appendLogTextArea(myGameEngine.getPlayerUp().getName() + " wins!");
    				}
    				myGameEngine.switchPlayerUp();
    			}
    			
    		}
    	
    	
    		if(!myGameEngine.getGameBoard().getIsAWin())
    		{
    			if (myGameEngine.getPlayerUp() == myGameEngine.getComPlayer())
    			{
    				myGameEngine.placePiece(myGameEngine.getGameBoard().findBestMoveColumn(myGameEngine.getComPlayer().getPieceType()));
    					
    				int compRow = (int)myGameEngine.getGameBoard().getLastPoint().getY() + 1;
    				
    				myView.changeImage((int)myGameEngine.getGameBoard().getLastPoint().getX(), myGameEngine.getGameBoard().getNumRows()- compRow, myGameEngine.getPlayerUp().getPieceType());
    				myView.appendLogTextArea(myGameEngine.getPlayerUp().getName() + " places piece at " + (int)myGameEngine.getGameBoard().getLastPoint().getX() + ", " + (int)myGameEngine.getGameBoard().getLastPoint().getY());
    					
    				if (myGameEngine.getGameBoard().checkIfWin()) 
    				{	
    					myGameEngine.getPlayerUp().incrementNumWins();
    					myView.setComPlayerWinsTextField(myGameEngine.getComPlayer().getNumWins());
   						myView.appendLogTextArea(myGameEngine.getPlayerUp().getName() + " wins!");
   					}
   				myGameEngine.switchPlayerUp();
    			}    		
    		}
    	} else if (myGameEngine.getGameBoard().getIsATie())
    	{
    		myView.wipeBoard();
    		myView.clearLogTextArea();
    		myGameEngine.getGameBoard().incrementTies();
    		myView.setNumTiesTextField(myGameEngine.getGameBoard().getNumTies());
    		myGameEngine.startGame();
    	} else
    	{
    		myView.wipeBoard();
    		myView.clearLogTextArea();
			myGameEngine.startGame();
    	}
    	
    }
    

    
    public GameEngine getMyGameEngine()
    {
    	return myGameEngine;
    }


    
}