package connectmodel;

import java.awt.Point;
import java.util.Arrays;
import java.util.Random;
import java.util.Vector;

/**
 * GameBoard class creates a 2-D array to represent Connect4 board and interacts with array
 * in order to place pieces and get wins
 * @author hudson
 *
 */
public class GameBoard
{
    private int myNumRows;
    private int myNumColumns;
    private PieceType[][] myBoard;
    private int myNumTypes;
    private int myWinLength;
    private Point myLastPoint;
    private Vector<PieceType> myTypes;
    private Point myWinBegin;
    private Point myWinEnd;
    private boolean myIsAWin;
    private int myConnectedPiece1;
    private int myConnectedPiece2;
    private int myNumTies;
	
    /**
     * Takes in the number of rows, number of columns, the required length to win, the different win types, and makes a game board
     * @param rows
     * @param cols
     * @param winLength
     * @param types
     * 
     * @author hudson
     */
    public GameBoard(int rows, int cols, int winLength, PieceType[] types) 
    {
    	myNumRows = rows;
    	myNumColumns = cols;
    	myBoard = new PieceType[cols][rows];
    	myTypes = new Vector<PieceType>(Arrays.asList(types));
    	myWinLength = winLength;
    	myIsAWin = false;
    	myConnectedPiece1 = 0;
    	myConnectedPiece2 = 0;
    	myNumTypes = myTypes.size();
    	myNumTies = 0;
    }
	
    /**
     * Takes in an int column and PieceType type in order to place a piece of PieceType type
     * to place in the column
     * 
     * @param col
     * @param type
     */
    public boolean placePiece(int col, PieceType type)
    {
	    if (col >= 0 && col < myNumColumns)
	    {
	    	if(myBoard[col][myNumRows - 1] == null)
	    	{
	    		for (int i = myNumRows; i > 0; i--)
	    		{
	    			if(myBoard[col][i-1] != null)
	    			{
	    				myBoard[col][i] = type;
	    				myLastPoint = new Point(col,i);
	    				return true;
	    			}
	    		}
	    		myBoard[col][0] = type;
	    		myLastPoint = new Point(col,0);
	    		return true;
	    	}
	    }
	    return false;
    }
	
    /**
     * Sets all points on the board equal to null
     */
    public void resetBoard() 
    {
    	for (int i = 0; i < myNumColumns; i++)
    	{
    		for (int t = 0; t < myNumRows; t++)
    		{
    			myBoard[i][t] = null;
    		}
    	}
    	myIsAWin = false;
    }
	
    /**
     * Returns the myNumTypes int
     * @return
     */
    public int getMyNumTypes() 
    {
    	return myNumTypes;
    }
    /**
     * Checks the three win booleans and returns true if any of the three are true
     * @return
     */
    public boolean checkIfWin() 
    {
	   if(checkVerticalWin() ||checkHorizontalWin() || checkDiagonalWin())
	   {
		   myIsAWin = true;
		   return true;
	   }
	   return false;
    }

	
    /**
     * Cycles through board with a for loop going through rows nestled within a for loop going through columns in order to check
     * if there is a continuous set of pieces that equals myWinLength.
     * @return
     */
    private boolean checkVerticalWin() 
    {
    	for(int x = 0; x < myNumColumns; x++)
    	{
    		myConnectedPiece1 = 0;
    		myConnectedPiece2 = 0;
    		for(int y = 0; y < myNumRows; y++)
    		{
    			if(checkConnectedPieces(x,y))
    			{
    				return true;
    			}
    		}
    	}
    	return false;
    }
	
    /**
     * Cycles through board with a for loop going through columns nested within a for loop going through the rows in order to 
     * check if there is a continuous set of pieces that equals myWinLength
     * @author hudson
     * @return boolean
     */
    private boolean checkHorizontalWin() 
    {
    	
    	for(int y = 0; y < myNumRows; y++)
    	{
    		myConnectedPiece1 = 0;
    		myConnectedPiece2 = 0;
    		for(int x = 0; x < myNumColumns; x++)
    		{
    			if(checkConnectedPieces(x,y))
    			{
    				return true;
    			}
    		}
    	}
    	return false;
    }
    
    /**
     * Calls on the two check diag win functions for each position on the board, one that checks from the top left to the bottom right and one that checks
     * from the bottom left to the top right
     * @author hudson
     * @return
     */
	private boolean checkDiagonalWin() 
	{
		if(myBoard != null)
		{
			for (int x = 0; x < myNumColumns; x++)
			{
				if(x == 0)
				{
					for(int y = 0; y < myNumRows; y++)
					{
						if( checkDiagSlopeUp(x,y) || checkDiagSlopeDown(x,y))
						{
							return true;
						}
					}
				} else
				{
					if(checkDiagSlopeUp(x,0) || checkDiagSlopeDown(x,myNumRows-1))
					{
						return true;
					}
				}
			}
		}
		return false;
	}

	/**
	 * Moves in a positive slope checking for diagonal wins from the position given.
	 * @param xPos
	 * @param yPos
	 * @return
	 */
	private boolean checkDiagSlopeUp(int xPos, int yPos)
	{
		int x = xPos;
		myConnectedPiece1 = 0;
		myConnectedPiece2 = 0;
		for(int y = yPos; y < myNumRows; y++)
		{
			if(x < myNumColumns)
			{
				if(checkConnectedPieces(x,y))
				{
					return true;
				}
				x++;
			}
		}
		return false;
	}
	
	/**
	 * Moves in a negative slope checking for diagonal wins from the position given.
	 * @param xPos
	 * @param yPos
	 * @return
	 */
	private boolean checkDiagSlopeDown(int xPos, int yPos)
	{
		int x = xPos;
		myConnectedPiece1 = 0;
		myConnectedPiece2 = 0;
		for(int y = yPos; y >= 0; y--)
		{
			if(x < myNumColumns)
			{
				if(checkConnectedPieces(x,y))
				{
					return true;
				}
				x++;
			}
		}
		return false;
	}
	
	/**
	 * Checks to see if piece is same as pieces checked before it and either increments or changes to zero the number of connected pieces, returning true if
	 * the number of connected pieces is equal to or greater than the win length.
	 * @param x
	 * @param y
	 * @return
	 */
	private boolean checkConnectedPieces(int x, int y)
	{
		if(myBoard[x][y] != null)
		{
			if(myTypes.get(0) == myBoard[x][y])
			{
				myConnectedPiece2 = 0;
				if(myConnectedPiece1 == 0)
				{
					myWinBegin = new Point(x,y);
				}
				
				if(myConnectedPiece1 < myWinLength)
				{
					myConnectedPiece1 += 1;
				}
				if(myConnectedPiece1 >= myWinLength)
				{
					myWinEnd = new Point(x,y);
					return true;
				}
			} else if(myTypes.get(1) == myBoard[x][y])
			{
				myConnectedPiece1 = 0;
				if(myConnectedPiece2 == 0)
				{
						myWinBegin = new Point(x,y);
					}
					
					if(myConnectedPiece2 < myWinLength)
					{
						myConnectedPiece2 += 1;
					}
					if(myConnectedPiece2 >= myWinLength)
					{
					myWinEnd = new Point(x,y);
					return true;
				}
			}
		} else
		{
			myConnectedPiece1 = 0;
			myConnectedPiece2 = 0;
		}
		return false;
	}
	
	
    /**
     * Takes in a piece and returns the best possible move for a piece of the given type. Simulates a piece falling in each column and uses those points 
     * to call on the countLength function, which measures the length of pieces placed by both player and opponent to return the best column to place a piece.
     * @param type
     * @return
     */
    public int findBestMoveColumn(PieceType type)
    {
    	if (myBoard != null)
    	{
    		Point[] myTestPoints = new Point[myNumColumns];
    		for (int x = 0; x < myNumColumns; x++)
    		{
    			if(myBoard[x][myNumRows - 1] == null)
    			{
    				for (int i = myNumRows-1; i >= 0; i--)
    				{
    					if (i == 0)
    					{
    						myTestPoints[x] = new Point(x,i);
    						break;
    					}
    					if(myBoard[x][i-1] != null)
    					{
    						
    						myTestPoints[x]= new Point(x,i);
    						break;
    					}
    				}
    			} else {
    				myTestPoints[x] = null;
    			}
    		}
    	
    		int myBestScore = 0;
    		int myBestPlacement = 0;
    		for(int i = 0; i < myTestPoints.length; i++)
    		{
    			if (myBestScore < countLength(myTestPoints[i], type) && myTestPoints[i] != null) 
    			{
    				myBestPlacement = (int)myTestPoints[i].getX();
    				myBestScore = countLength(myTestPoints[i], type);
    			}
    		}
    		if(myBestScore == 0) 
    		{
    			Random random = new Random();
    			return random.nextInt(myNumColumns-1);
    		} else
    		{
    			return myBestPlacement;
    		}
    	}
		Random random = new Random();
		return random.nextInt(myNumColumns-1);
    }
    
    /**
     * Calls on horizontal, vertical, diagonal length negative slope, and diagonal length positive slope length functions and adds the score of each of them together to
     * return a score for a given point and a given piece type.
     * @author hudson
     * @param point
     * @param type
     * @return
     */
    private int countLength(Point point, PieceType type)
    {
    	if (point != null )return (countHorizontalLengthIfPiecePlaced(point,type) + countVerticalLengthIfPiecePlaced(point,type) + countDiagonalLengthIfPiecePlaced(point,type));
    	else return 0;
    }
    
    private int countHorizontalLengthIfPiecePlaced(Point point, PieceType type) 
    {
    	int selfConnectedLeft = 0;
    	int selfConnectedRight = 0;
    	int otherConnectedLeft = 0;
    	int otherConnectedRight = 0;
    	for (int x = 0; x < (int)point.getX(); x++)
    	{
    		if(myBoard[x][(int)point.getY()] == type)
   			{
   				selfConnectedLeft+= 1;
   				otherConnectedLeft = 0;
   			}
    		if (myBoard[x][(int)point.getY()] != type)
    		{
    			if(myBoard[x][(int)point.getY()] != null)
    			{
   					otherConnectedLeft += 1;
   				} else 
   				{
   					otherConnectedLeft = 0;
    			}
    			selfConnectedLeft = 0;
    		}
    	}
    	
    	for (int x = myNumColumns-1; x > (int)point.getX(); x--)
    	{
    		if(myBoard[x][(int)point.getY()] == type)
   			{
   				selfConnectedRight+= 1;
   				otherConnectedRight = 0;
   			}
    		if (myBoard[x][(int)point.getY()] != type)
    		{
    			if(myBoard[x][(int)point.getY()] != null)
    			{
   					otherConnectedRight += 1;
   				} else 
   				{
   					otherConnectedRight = 0;
    			}
    			selfConnectedRight = 0;
    		}
    	}
    	
    	if(selfConnectedLeft + selfConnectedRight >= myWinLength - 1)
    	{
    		
    		return 1000;
    	} else if(otherConnectedLeft + otherConnectedRight >= myWinLength -1)
    	{
    		return 50;
    	} else return (((selfConnectedLeft + selfConnectedRight) * 2) + otherConnectedLeft + otherConnectedRight);

    }
	
    /**
     * Measures the vertical length of pieces placed by both the given piece type and the opposing piece type for a given point and returns 
     * a score based on the number of connected friendly pieces, number of connected opponent pieces, whether the placement can cause a win,
     * and whether the placement can stop the opponent from winning.
     * @author hudson
     * @param point
     * @param type
     * @return
     */
    private int countVerticalLengthIfPiecePlaced(Point point, PieceType type) 
    {
    	int selfConnectedUp = 0;
    	int selfConnectedDown = 0;
    	int otherConnectedUp = 0;
    	int otherConnectedDown = 0;
    	
    	int x = (int)point.getX();
    	for (int y = 0; y < (int)point.getY(); y++)
    	{
    		if(myBoard[(int)point.getX()][y] == type)
   			{
   				selfConnectedUp+= 1;
   				otherConnectedUp = 0;
   			}
    		if (myBoard[x][y] != type)
    		{
    			if(myBoard[x][y] != null)
    			{
   					otherConnectedUp += 1;
   				} else 
   				{
   					otherConnectedUp = 0;
    			}
    			selfConnectedUp = 0;
    		}
    	}
    	
    	for (int y = myNumRows-1; y > (int)point.getY(); y--)
    	{
    		if(myBoard[x][y] == type)
   			{
   				selfConnectedDown+= 1;
   				otherConnectedDown = 0;
   			}
    		if (myBoard[x][y] != type)
    		{
    			if(myBoard[x][y] != null)
    			{
   					otherConnectedDown += 1;
   				} else 
   				{
   					otherConnectedDown = 0;
    			}
    			selfConnectedDown = 0;
    		}
    	}
    	
    	if(selfConnectedUp + selfConnectedDown >= myWinLength - 1)
    	{
    		return 1000;
    	} else if(otherConnectedUp + otherConnectedDown >= myWinLength -1)
    	{
    		return 50;
    	} else return (((selfConnectedUp + selfConnectedDown) * 2) + otherConnectedUp + otherConnectedDown);
    }
	
    /**
     * Uses the two separate diagonal functions (one sloping up and one sloping down) with the given point and type to return a score of the two functions added together
     * @param point
     * @param type
     * @return
     */
    private int countDiagonalLengthIfPiecePlaced(Point point, PieceType type) 
    {
    	return (countDiagonalLengthIfPiecePlacedSlopeUp(point, type) + countDiagonalLengthIfPiecePlacedSlopeDown(point, type));
    }
	
    /**
     * Measures the diagonal length sloping up of pieces placed by both the given piece type and the opposing piece type for a given point and returns 
     * a score based on the number of connected friendly pieces, number of connected opponent pieces, whether the placement can cause a win,
     * and whether the placement can stop the opponent from winning.
     * @author hudson
     * @param point
     * @param type
     * @return
     */
    private int countDiagonalLengthIfPiecePlacedSlopeUp(Point point, PieceType type) 
    {
    	int selfConnectedLeft = 0;
    	int selfConnectedRight = 0;
    	int otherConnectedLeft = 0;
    	int otherConnectedRight = 0;
    	
    	int x = (int)point.getX();
    	
    	
    	for(int y = (int)point.getY(); y >= 0; y--)
		{
			if(x >= 0)
			{
				if(myBoard[x][y] == type)
	   			{
	   				selfConnectedLeft+= 1;
	   				otherConnectedLeft= 0;
	   			}
	    		if (myBoard[x][y] != type)
	    		{
	    			if(myBoard[x][y] != null)
	    			{
	   					otherConnectedLeft += 1;
	   				} else 
	   				{
	   					otherConnectedLeft = 0;
	    			}
	    			selfConnectedLeft = 0;
	    		}
				x--;
			}
		}
    	
    	x = (int)point.getX();
    	for(int y = (int)point.getY(); y < myNumRows; y++)
		{
			if(x < myNumColumns)
			{
				if(myBoard[x][y] == type)
	   			{
	   				selfConnectedRight+= 1;
	   				otherConnectedRight = 0;
	   			}
	    		if (myBoard[x][y] != type)
	    		{
	    			if(myBoard[x][y] != null)
	    			{
	   					otherConnectedRight += 1;
	   				} else 
	   				{
	   					otherConnectedRight = 0;
	    			}
	    			selfConnectedRight = 0;
	    		}
				x++;
			}
		}
    	

    	
    	if(selfConnectedLeft + selfConnectedRight >= myWinLength - 1)
    	{
    		return 1000;
    	} else if(otherConnectedLeft + otherConnectedRight >= myWinLength -1)
    	{
    		return 50;
    	} else return (((selfConnectedLeft + selfConnectedRight) * 2) + otherConnectedLeft + otherConnectedRight);
    }
    
    /**
     * Measures the diagonal length sloping down of pieces placed by both the given piece type and the opposing piece type for a given point and returns 
     * a score based on the number of connected friendly pieces, number of connected opponent pieces, whether the placement can cause a win,
     * and whether the placement can stop the opponent from winning.
     * @author hudson
     * @param point
     * @param type
     * @return
     */
    private int countDiagonalLengthIfPiecePlacedSlopeDown(Point point, PieceType type) 
    {
    	int selfConnectedLeft = 0;
    	int selfConnectedRight = 0;
    	int otherConnectedLeft = 0;
    	int otherConnectedRight = 0;
    	
    	int x = (int)point.getX();
    	for(int y = (int)point.getY(); y >= 0; y--)
		{
			if(x < myNumColumns)
			{
				if(myBoard[x][y] == type)
	   			{
	   				selfConnectedLeft+= 1;
	   				otherConnectedLeft= 0;
	   			}
	    		if (myBoard[x][y] != type)
	    		{
	    			if(myBoard[x][y] != null)
	    			{
	   					otherConnectedLeft += 1;
	   				} else 
	   				{
	   					otherConnectedLeft = 0;
	    			}
	    			selfConnectedLeft = 0;
	    		}
				x++;
			}
		}
    	
    	x = (int)point.getX();
    	for(int y = (int)point.getY(); y < myNumRows; y++)
		{
			if(x >= 0)
			{
				if(myBoard[x][y] == type)
	   			{
	   				selfConnectedRight+= 1;
	   				otherConnectedRight = 0;
	   			}
	    		if (myBoard[x][y] != type)
	    		{
	    			if(myBoard[x][y] != null)
	    			{
	   					otherConnectedRight += 1;
	   				} else 
	   				{
	   					otherConnectedRight = 0;
	    			}
	    			selfConnectedRight = 0;
	    		}
				x--;
			}
		}
    	

    	
    	if(selfConnectedLeft + selfConnectedRight >= myWinLength - 1)
    	{
    		return 1000;
    	} else if(otherConnectedLeft + otherConnectedRight >= myWinLength -1)
    	{
    		return 50;
    	} else return (((selfConnectedLeft + selfConnectedRight) * 2) + otherConnectedLeft + otherConnectedRight);
    }
    
    
    /**
     * Returns the content of the myTypes vector if it is not empty.
     * @return
     */
    public Vector<PieceType> getTypes() 
    {
	    if (!myTypes.isEmpty())
	    {
	    	return myTypes;
	    } else 
	    {
	    	return null;
	    }
    }
    /**
     * Returns the starting win position
     * @return
     */
    public Point getWinBegin() 
    {
	    if (myWinBegin != null)
	    {
	    	return myWinBegin;
	    }
	    return null;
    }
	
    /**
     * Returns the ending win position
     * @return
     */
    public Point getWinEnd()
    {
	    if (myWinEnd != null)
	    {
	    	return myWinEnd;
	    }
	    return null;
    }
	
    /**
     * Returns the point of the last piece placed
     * @return
     */
    public Point getLastPoint() 
    {
	    if (myLastPoint != null)
	    {
	    	return myLastPoint;
	    }
	    return null;
    }
	
    /**
     * Returns the piece on the board at a specified point
     * @param point
     * @return
     */
    public PieceType getPieceOnBoard(Point point) 
    {
    	if (point.x > 0 && point.x < myNumColumns && point.y > 0 && point.y < myNumRows)
    	{
	    return myBoard[point.x][point.y];
    	}
    	return null;
    }
	
    /**
     * Returns the entire board
     * @return
     */
    public PieceType[][] getBoard() 
    {
	    if (myBoard != null)
	    {
	    	return myBoard;
	    }
	    return null;
    }
	
    /**
     * Checks to see if the board is full
     * @return
     */
    public boolean isBoardFull() 
    {
    	for (int i = 0; i < myNumColumns; i++)
    	{
    		for (int t = 0; t < myNumRows; t++)
    		{
    			if (myBoard[i][t] == null)
    			{
    				return false;
    			}
    		}
    	}
    	return true;
    }
	
    /**
     * Checks to see if a given column is full
     * @param col
     * @return
     */
    public boolean isColumnFull(int col) 
    {
    	if (col >= 0 && col < myNumColumns)
    	{
    		for (int t = 0; t < myNumRows; t++)
    		{
    			if (myBoard[col][t] == null)
    			{
    				return false;
    			}
    		}
    		return true;
    	}
    	return false;
    }
	
    /**
     * Returns the myIsAWin boolean
     * @return
     */
    public boolean getIsAWin() 
    {
    	return myIsAWin;
    }
    
    /**
     * Checks to see if the entire board is null (empty)
     * @return
     */
    public boolean checkAllNull()
    {
    	for (int i = 0; i < myNumColumns - 1; i++)
    	{
    		for (int t = 0; t < myNumRows - 1; t++)
    		{
    			if (myBoard[i][t] != null)
    			{
    				return false;
    			}
    		}
    	}
    	return true;
    }
    
    /**
     * returns the number of columns
     */
    public int getNumColumns()
    {
    	return myNumColumns;
    }
    
    /**
     * returns the number of rows
     */
    public int getNumRows()
    {
    	return myNumRows;
    }
    
    public boolean getIsATie()
    {
    	if (isBoardFull() && !myIsAWin)
    	{
    		return true;
    	}
    	return false;
    }
    
    public void incrementTies()
    {
    	myNumTies += 1;
    }
    
    public int getNumTies()
    {
    	return myNumTies;
    }
}