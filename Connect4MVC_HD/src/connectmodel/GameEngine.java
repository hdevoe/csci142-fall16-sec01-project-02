package connectmodel;

import java.util.Vector;
/**
 * GameEngine class is responsible for cycling through players and getting their piece placements
 * @author hudson
 *
 */

public class GameEngine 
{
	private Vector<Player> myPlayers;
	private Player myPlayerUp;
	private ComputerPlayer myComPlayer;
	private Player myStartingPlayer;
	private GameBoard myGameBoard;
  
	/**
	 * Creates a Game Engine with given player and game board. Creates a computer player with non-selected color + adds both player and computer player 
	 * to myPlayers vector.
	 * @param myPlayer1
	 * @param gameBoard
	 */
	public GameEngine(Player myPlayer1, GameBoard gameBoard) 
	{
		myGameBoard = gameBoard;
		if (myGameBoard != null && myPlayer1 != null){
			if(myPlayer1.getPieceType() == myGameBoard.getTypes().get(0))
			{
				myComPlayer = new ComputerPlayer("COM", myGameBoard.getTypes().get(1));
			} else if(myPlayer1.getPieceType() == myGameBoard.getTypes().get(1))
			{
				myComPlayer = new ComputerPlayer("COM", myGameBoard.getTypes().get(0));
			}
		
			myPlayers = new Vector<Player>(2);
			myPlayers.add(myPlayer1);
			myPlayers.add(myComPlayer);
			myPlayerUp = myPlayers.get(0);
			myStartingPlayer = myPlayerUp;
		}
	}

	/**
	 * Selects Starting player from the myPlayers vector.
	 * @param player
	 * @return
	 */
	public boolean selectStartingPlayer(Player player)
	{
		if(checkValidPlayerArray()) 
		{
			int i;
			for(i = 0; i < myPlayers.size()-1;i++)
			{
				if (myPlayers.get(i) == player)
				{
					myStartingPlayer = myPlayers.get(i);
					myPlayerUp = myStartingPlayer;
					return true;
				}
			}
		}
		return false;
	}
	
	/**
	 * Starts the game if the game board and myPlayers array are not null.
	 * @return
	 */
	public boolean startGame() 
	{
		if(myGameBoard != null && myPlayers != null)
		{
			myGameBoard.resetBoard();
			return true;
		}
		return false;
	}
	
	/**
	 * Switches the player up to the other player in the myPlayers vector.
	 * @return
	 */
	public Player switchPlayerUp() 
	{
		if(checkValidPlayerArray())
		{
			if (myPlayerUp == myPlayers.get(0)) 
			{
				myPlayerUp = myPlayers.get(1);
				return myPlayerUp;
			}
			else if(myPlayerUp == myPlayers.get(1))
			{
				myPlayerUp = myPlayers.get(0);
				return myPlayerUp;
			}
		}
		return null;

	}

	/**
	 * If the myGameBoard and myPlayerUp variables are not null, uses the myGameBoard.placePiece function to either place a piece and return true
	 * or not place a piece and return false.
	 * @param column
	 * @return
	 */
	public boolean placePiece(int column)
	{
		if (myPlayerUp != null && myGameBoard != null)
		{
			return myGameBoard.placePiece(column, myPlayerUp.getPieceType());
		}
		return false;
	}

	/**
	 * Returns the current player up.
	 * @return
	 */
	public Player getPlayerUp() 
	{
		if (myPlayerUp != null)
		{
			return myPlayerUp;
		}
		return null;
	}
	
	/**
	 * returns the myStartingPlayer variable
	 * @return
	 */
	public Player getStartingPlayer()
	{
		return myStartingPlayer;
	}

	/**
	 * returns the myPlayers vector if the vector is not empty
	 * @return
	 */
	public Vector<Player> getPlayers()
	{
		if (!myPlayers.isEmpty())
		{
			return myPlayers;
		}
		return null;
	}
  
	/**
	 * Sets the myGameBoard variable to equal gameboard.
	 * @param gameboard
	 */
	public void setGameBoard(GameBoard gameboard)
	{
		myGameBoard = gameboard;
	}

	/**
	 * Returns the myGameBoard[]s[] if it isn't null.
	 * @return
	 */
	public GameBoard getGameBoard()
	{
		if (myGameBoard != null)
		{
			return myGameBoard;
		}
		return null;
	}
	
	/**
	 * Checks if the myPlayers array is valid and then returns it
	 * @return
	 */
	private boolean checkValidPlayerArray()
	{
		for(int i = 0; i < myPlayers.size() - 1; i++)
		{
			if(myPlayers.get(i) != null)
			{					
				return true;
			}
		}
		return false;
	}
	
	public Player getMyPlayer1()
	{
		return myPlayers.get(0);
	}
	
	public Player getComPlayer()
	{
		return myPlayers.get(1);
	}
}