package connectmodel;

/**
 * PieceType Enum holds the different types of pieces available for player
 * @author hudson
 *
 */
public enum PieceType 
{
	RED ("Red"),
	BLACK ("Black"),
	GREEN ("Green"),
	YELLOW ("Yellow");
    
	private String myType;
	
    private PieceType(String type) 
    {
    	myType = type;
    }
    
    /**
     * Returns the myType string
     * @return
     */
    public String getType()
    {
    	return myType;
    }
    
    public static PieceType type(String type)
    {
    	if (type.equals("Red"))
    	{
    		return PieceType.RED;
    	}
    	
    	if (type.equals("Black"))
    	{
    		return PieceType.BLACK;
    	}
    	
    	if (type.equals("Green"))
    	{
    		return PieceType.GREEN;
    	}
    	
    	if (type.equals("Yellow"))
    	{
    		return PieceType.YELLOW;
    	}
    	
    	return null;
    }
}