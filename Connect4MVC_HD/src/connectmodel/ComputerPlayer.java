package connectmodel;

/**
 * The ComputerPlayer class extends class Player and picks a best position 
 * based on the current board
 * @author hudson
 *
 */
public class ComputerPlayer extends Player
{
	public ComputerPlayer(String name, PieceType type)
	{
		super(name, type);
	}
} 